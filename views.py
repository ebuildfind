from django.shortcuts import render_to_response
from planet import Parser

from models import Ebuild

import re, string

PUNCTUATION_REGEX = re.compile('[' + re.escape(string.punctuation.replace("-", "").replace("+", "")) + ']')

def index(request):
    GPlanet = Parser("http://planet.gentoo.org/atom.xml")
    GOverlays = Parser("http://overlays.gentoo.org/rss20.xml")
    GNews = Parser("http://www.gentoo.org/rdf/en/gentoo-news.rdf")
    identica = Parser("https://identi.ca/api/laconica/groups/timeline/gentoo.atom")

    response = dict()
    
    response['identica'] = identica
    response['GNews'] = GNews
    response['GOverlays'] = GOverlays
    response['GPlanet'] = GPlanet

    return render_to_response("ebuildfind/index.html", response)
