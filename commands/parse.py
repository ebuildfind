from __future__ import with_statement

import os, re

from django.conf import settings

os.environ['DJANGO_SETTINGS_MODULE'] = 'ebuilds.settings'

from layman.debug import Message

from ebuilds.ebuildfind.models import Ebuild, Overlay

from manage_layman import LaymanManager

OVERLAYS_BASE = settings.ROOT_PATH + "ebuilds/" + "ebuildfind/commands/var/overlays/"

class EbuildParser:
    def __init__(self, overlay, category, app, file_path):
        path, filename = os.path.split(os.path.realpath(file_path))
        
        name_version, ext = os.path.splitext(filename)
        n = len(app)
        
        version = name_version[n+1:]
        
        self.name = app
        self.version = version
        self.category = category
        self.overlay = overlay
        
        with open(file_path) as file:
            f = file.read()
            regex1 = re.compile(r'([a-z_]+\s*\(\))\s*{.*}', re.DOTALL)
            f = re.sub(regex1, r'', f)
            regex2 = re.compile(r'(for.*done)', re.DOTALL)
            f = re.sub(regex2, r'', f)
            data = dict(re.findall("(DESCRIPTION|HOMEPAGE|KEYWORDS|LICENSE|IUSE)=\"(.+)\"", f))

        def notnull(d, key):
            try:
                return d[key]
            except:
                return ""

        self.description = notnull(data, "DESCRIPTION")
        self.description = self.description.replace("\n"," ")
        self.homepage = notnull(data, "HOMEPAGE")
        self.keywords = notnull(data, "KEYWORDS")
        self.license = notnull(data, "LICENSE")
        self.iuse = notnull(data, "IUSE")

    def __repr__(self):
        output = "%s/%s [%s] @ %s" % (self.category, self.name, self.version, self.overlay)
        output += " " + self.description
        output += " " + self.homepage
        output += " " + self.keywords
        output += " " + self.license
        output += " " + self.iuse
        return output

def exclude_directory(path, dir):
    exclude_dir = ["eclass", "profiles", "README-TODO", "Documentation", "sets", "index"]
    return os.path.isdir(path) and dir not in exclude_dir and not dir.startswith(".")

def ParseEbuilds():
    i = 0    
    Ebuild.objects.all().delete()
    overlays = os.listdir(OVERLAYS_BASE)
  
    for overlay in overlays:
        path_overlay = os.path.join(OVERLAYS_BASE, overlay)

        if exclude_directory(path_overlay, overlay):
            overlay_name = overlay
            print "is present", overlay_name
            
            overlay = Overlay.objects.get(name=overlay)
            
            categories = os.listdir(path_overlay)
                                      
            for category in categories:
                path_overlay_category = os.path.join(path_overlay, category)

                if exclude_directory(path_overlay_category, category):
                    apps = os.listdir(path_overlay_category)
                    for app in apps:
                        path_overlay_category_app = os.path.join(path_overlay_category, app)
                        if exclude_directory(path_overlay_category_app, app):
                            ebuilds = os.listdir(path_overlay_category_app)
                        
                            for ebuild in ebuilds:
                                if ebuild.endswith(".ebuild"):
                                    if exclude_directory(path_overlay_category_app, ebuild):
                                        
                                        path_ebuild = os.path.join(path_overlay_category_app, ebuild)                                 
                                    
                                        e = EbuildParser(overlay_name, category, app, path_ebuild)

                                    
                                        ebuild = Ebuild()
                                        ebuild.name = unicode(e.name)
                                        ebuild.category = unicode(e.category)
                                        ebuild.version = unicode(e.version)
                                        ebuild.description = unicode(e.description, errors="replace")
                                        ebuild.keywords = unicode(e.keywords)
                                        ebuild.license = unicode(e.license)
                                        ebuild.iuse = unicode(e.iuse)
                                        ebuild.homepage = unicode(e.homepage)
                                        ebuild.overlay = overlay
                                        ebuild.save()

                                        ebuild.index()
    
def ParseOverlays():
    h = LaymanManager()
    overlays = h.List()
    
    for name, overlay in overlays.items():
        """ check if new overlay is ready """
        o = Overlay.objects.all().filter(name=name)
	print "add ? > ", name
        if not o:
            print "added !", name
            o = Overlay()
            o.name = name
            o.description = overlay["description"]
            o.link = overlay["link"]
            o.save()
                    
def main():
    print "# >>> Parse Overlays"
    ParseOverlays()
    print "# >>> Parse Ebuilds"    
    ParseEbuilds()
    
if __name__ == "__main__":
    main()
