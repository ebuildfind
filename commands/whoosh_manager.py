import os

from whoosh import store
from whoosh.fields import Schema, TEXT, ID
from whoosh.index import Index
from whoosh.qparser import QueryParser

from shutil import rmtree

from django.conf import settings

os.environ['DJANGO_SETTINGS_MODULE'] = 'ebuilds.settings'

INDEX_BASE = settings.ROOT_PATH + "ebuilds" + "/ebuildfind/commands/var/index"

class WhooshEbuildManager:
    def __init__(self, reset=False):
        if reset :
            if os.path.exists(INDEX_BASE):
                rmtree(INDEX_BASE)
            os.mkdir(INDEX_BASE)
            storage = store.FileStorage(INDEX_BASE)
        else:
            storage = store.FileStorage(INDEX_BASE)
            storage = store.copy_to_ram(storage)
        
        schema = Schema(permalink=ID(stored=True),
                        content=TEXT(phrase=False))
        
        self.index = Index(storage, schema=schema, create=reset)
        self.searcher = self.index.searcher()
        self.parser = QueryParser("content", schema = self.index.schema)
        
    def Update(self, ebuild):
        writer = self.index.writer()
        
        def clean(text):
            return text.lower.replace("-", " ")
        
        content_raw = [ebuild.overlay.name, ebuild.category, ebuild.name, ebuild.description]
        content = []
        
        for e in content_raw:
            e = e.lower().replace("-", " ")
            content.append(e)

        content.append(ebuild.version)
        
        content = ' '.join(content)

        writer.add_document(permalink=unicode(ebuild.id),
                            content=content)
        print ">", ebuild.overlay.name, ">>> ", ebuild.name
        writer.commit()

    def Search(self, query):
        return self.searcher.search(self.parser.parse(query), sortedby = "permalink")
