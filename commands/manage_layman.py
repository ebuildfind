from subprocess import call

from commands import getstatusoutput

import sys, ConfigParser, os.path, re

from layman.config import Config
from layman.action import *
from layman.db import DB

here = os.path.split(os.path.realpath(__file__))[0]

class LaymanManager:
    def AddAll(self):
        """add overlays and sync them"""
        
        command1 = "./layman -c mylayman.cfg -a ALL"
        print command1
        (status, output) = getstatusoutput(command1)
        print status
        print output
        
        """
        command2 = "./layman -c mylayman.cfg -s ALL"
        getstatusoutput(command2)

        print command1, "\n", command2
        """
        
    def List(self):
        """return overlays dict"""

        sys.argv.append('-c')
        sys.argv.append('mylayman.cfg')
        config = Config()
        
        """
        l = ListLocal(a)
        l.run()
        """

        db = DB(config)
        overlays = dict()
        
        for name, overlay in db.overlays.items():
            overlays[name] = dict()
            overlays[name]["src"] = overlay.src
            overlays[name]["contact"] = overlay.contact
            
            description = overlay.description
            description = re.compile(u' +').sub(u' ', description)
            description = re.compile(u'\n ').sub(u'\n', description)
            
            overlays[name]["description"] = description
            
            if '<link>1' in overlay.data.keys():
                link = overlay.data['<link>1']['@'].strip()
                link = re.compile(u' +').sub(u' ', link)
                link = re.compile(u'\n ').sub(u'\n', link)
            else:
                link = ""
            
            overlays[name]["link"] = link
        return overlays

def main():
    h = LaymanManager()
    h.AddAll()
    overlays = h.List()
    
if __name__ == '__main__':
    main()
