#!/usr/bin/python
# -*- coding: utf-8 -*-
#################################################################################
# LAYMAN VERSION
#################################################################################
# File:       version.py
#
#             Current version number
#
# Copyright:
#             (c) 2005 - 2008 Gunnar Wrobel
#             Distributed under the terms of the GNU General Public License v2
#
# Author(s):
#             Gunnar Wrobel <wrobel@gentoo.org>
#

__version__ = "$Id: version.py 309 2007-04-09 16:23:38Z wrobel $"


VERSION = '1.2.3'

if __name__ == '__main__':
    print VERSION
