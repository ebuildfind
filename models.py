import re, string

from django.db import models

class Overlay(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField()
    link = models.URLField()

    def __unicode__(self):
        return self.name


class Ebuild(models.Model):
    name = models.CharField(max_length=255)
    category = models.CharField(max_length=255)
    version = models.CharField(max_length=255)
    description = models.TextField()
    keywords = models.TextField(max_length=255)
    license = models.TextField(max_length=255)
    iuse = models.TextField(max_length=255)
    homepage = models.URLField()
    overlay = models.ForeignKey(Overlay)
    
    def path(self):
        return "/%s/%s/%s/%s" % (self.overlay.name, self.category, self.name, self.version)

    def get_absolute_url(self):
	return "/search/?q=%s" % self.name

    def __unicode__(self):
        return self.name
