from django.conf.urls.defaults import *
from django.contrib.sitemaps import GenericSitemap
from models import Ebuild
import views


#from django.contrib import admin

info_dict = {
    'queryset': Ebuild.objects.all(),
}

sitemaps = {
    'ebuild': GenericSitemap(info_dict),
}

urlpatterns = patterns('',
    (r'^sitemap.xml$', 'django.contrib.sitemaps.views.index', {'sitemaps': sitemaps}),
    (r'^sitemap-(?P<section>.+)\.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': sitemaps}),
    (r'^search/', include('haystack.urls')),
    (r'^$', views.index),
#    (r'^admin/(.*)', admin.site.root),
)
