import shelve

from feedcache.cache import Cache

from django.conf import settings

from django.template.defaultfilters import truncatewords_html


def TryEncoding(content):
    for body_charset in 'UTF-8', 'US-ASCII', 'ISO-8859-1', :
        try:
            return content.encode(body_charset)
        except UnicodeError:
            pass
        except Exception, e:
            if not type(content) is str:
                pass

def GetContent(feed):
    if hasattr(feed, "content"):
        return feed.content[0]["value"]
    else:
        return feed.summary
    
class Parser:
    """
    "http://planet.gentoo.org/atom.xml"
    "http://overlays.gentoo.org/rss20.xml"
    >>> f = Parser("http://www.gentoo.org/rdf/en/gentoo-news.rdf")
    >>> f.GetTitle()
    u'Planet Gentoo'
    >>> f.GetLink()
    u'http://planet.gentoo.org/'
    >>> for e in f: print e["title"], e["content"]
    """
    
    def __init__(self, url, summary=False):
	storage = shelve.open(settings.ROOT_PATH + "feedcache")
	try :
	    fc = Cache(storage)
	    self.feed = fc.fetch(url)
            self.iterator = 0
            self.summary = summary
        finally:
	    storage.close()
    def GetTitle(self):
        return self.feed.feed.title

    def GetLink(self):
        return self.feed.feed.link

    def next(self):
        if self.iterator >= len(self.feed.entries): raise StopIteration

        entry = self.feed.entries[self.iterator]
        
        title = TryEncoding(entry["title"])
        content = TryEncoding(GetContent(entry))
        link = entry.link

        if self.summary:
            content = TryEncoding(truncatewords_html(content, 30))
            content = "".join((content , "..."))

        entry = dict((("title", title), ("content", content), ("link", link)))
        
        self.iterator += 1
        return entry

    def __iter__(self):
        return self
