import datetime
from haystack import indexes
from haystack import site
from models import Ebuild

class EbuildIndex(indexes.SearchIndex):
    text = indexes.CharField(document=True, use_template=True)
site.register(Ebuild, EbuildIndex)
